import React, { useRef, useEffect } from 'react';
import { useLocation, Switch } from 'react-router-dom';
import AppRoute from './utils/AppRoute';
import ScrollReveal from './utils/ScrollReveal';
import ReactGA from 'react-ga';
import 'bootstrap/dist/css/bootstrap.min.css';
// Layouts
import LayoutDefault from './layouts/LayoutDefault';

// Views 
import Home from './views/Home';
import Player from './views/Player';
import Match from './views/Match';
import Media from './views/Media';
import Store from './views/Store';
import Detail from './views/Detail';
import PlayerDetail from './views/PlayerDetail';
import ProductDetails from './views/ProductDetails';

// Initialize Google Analytics
ReactGA.initialize(process.env.REACT_APP_GA_CODE);

const trackPage = page => {
  ReactGA.set({ page });
  ReactGA.pageview(page);
};

const App = () => {

  const childRef = useRef();
  let location = useLocation();

  useEffect(() => {
    const page = location.pathname;
    document.body.classList.add('is-loaded')
    childRef.current.init();
    trackPage(page);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);

  return (
    <ScrollReveal
      ref={childRef}
      children={() => (
        <Switch>
          <AppRoute exact path="/" component={Home} layout={LayoutDefault} />
          <AppRoute path="/player/:id" component={PlayerDetail} layout={LayoutDefault} />
          <AppRoute path="/player" component={Player} layout={LayoutDefault} />
          <AppRoute path="/match" component={Match} layout={LayoutDefault} />
          {/* <AppRoute path="/media" component={Media} layout={LayoutDefault} /> */}
          <AppRoute path="/store/:id" component={ProductDetails} layout={LayoutDefault} />
          <AppRoute path="/store" component={Store} layout={LayoutDefault} />
          <AppRoute path="/detail/:id" component={Detail} layout={LayoutDefault} />
          <AppRoute path="/media" component={Media} layout={LayoutDefault} />
        </Switch>
      )} />
  );
}

export default App;