import React from 'react';
import Header from '../components/layout/Header';
import Footer from '../components/layout/Footer';
import Hello from '../views/ChatBot';

const LayoutDefault = ({ children }) => (
  <>
    <Header navPosition="right" className="reveal-from-bottom" />
    <main className="site-content">
      {children}
      <Hello />
    </main>
    <Footer />
  </>
);

export default LayoutDefault;  