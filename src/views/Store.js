import React, { useEffect, useState } from "react";
import { Container, Col, Row, Image } from "react-bootstrap";
import { useHistory } from 'react-router-dom'

// ======== service ======
import { get_all_product } from "../services/product.services";


function ProductCard({ data }) {
  
  const history = useHistory();

  return (
    <>
      <div 
        className="wrap-product-card" 
        onClick={() => history.push('/store/' + data?.id)}
      >
        <Image className="photo" alt="Player Image" src={data?.images} />
        <div className="wrap-price-name">
          <h5 style={{ color: "white" }}>EOP {data?.name}</h5>
          <h5 style={{ color: "white" }}>{data?.price}</h5>
        </div>
      </div>
    </>
  );
}

const Store = () => {

  const [allProducts, setAllProducts] = useState();

  const onGetAllProducts = async () => {
    const products = await get_all_product();
    setAllProducts(products);
  };

  console.log("=================> product :", allProducts);

  useEffect(() => {
    onGetAllProducts();
  }, []);


  return (
    <Container>
      <h4 className="main-title-header" style={{ marginTop: "100px" ,marginBottom:'20px' }}>
        EOP Merchandise
      </h4>
      <div className="wrap-all-card">
        {allProducts?.map((val) => (
          <ProductCard data={val} />
        ))}
      </div>
      <div style={{marginTop:'1.5em'}}/>
    </Container>
  );
};
export default Store;
