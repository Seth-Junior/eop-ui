import React, { useEffect, useState } from 'react'
import { Container, Image, Table } from 'react-bootstrap';
import { useLocation, useParams } from 'react-router-dom';
import { get_View_Match_ById } from '../services/match.services';
import { FacebookIcon, FacebookShareButton } from "react-share";


function Detail() {
    let getId = useParams();
    // const getMatchId = location.state;
    console.log("Pass state : ", getId.id);
    const [matchView, setMatchView] = useState();

    const onGetViewMatchById = async () => {
        let data = await get_View_Match_ById(getId.id);
        setMatchView(data)
    }
    useEffect(() => {
        onGetViewMatchById();
    }, [])

    const theResult = matchView?.result && matchView?.result.split(" ");
    console.log("THIS IS MACTH VIEW DATA : ", theResult);


    return (
        <Container style={{ marginTop: "100px", textAlign: 'center', marginBottom: '50px' }}>
            <h4 className="main-title-header" style={{ marginTop: "10px", marginBottom:'20px' }}>
                {matchView && matchView?.type}
            </h4>
            <Table striped hover responsive>
                <tbody>
                    {/* <tr style={{backgroundColor: '#0047AB'}}>
                        <td colSpan={2}>{matchView && matchView?.type}</td>
                    </tr> */}
                    <tr>
                        <td style={{ width: '40%', height: '100px' }}>
                            <div style={{ width: '100%', height: '100px' }}>
                                <Image src={matchView && matchView?.elogo}
                                    style={{ width: "auto", height: "100px", display: 'block', marginLeft: "auto", marginRight: 'auto' }}
                                />
                            </div>
                        </td>
                        <td>
                            <div style={{ display: 'flex', width: '100%', height: '100px', justifyContent: 'center', alignItems: 'center' }}>
                                <h3 style={{ margin: '0px' }}>{matchView && matchView?.result}</h3>
                            </div>
                        </td>
                        <td style={{ width: '40%', height: '100px' }}>
                            <div style={{ width: '100%', height: '100px' }}>
                                <Image src={matchView && matchView?.logo}
                                    style={{ width: "auto", height: "100px", display: 'block', marginLeft: "auto", marginRight: 'auto' }}
                                />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>{matchView && matchView?.ename}</td>
                        <td>{matchView && matchView?.time}</td>
                        <td>{matchView && matchView?.name}</td>
                    </tr>
                    {/* <tr>
                        
                    </tr> */}
                    <tr>
                        <td>{matchView && matchView?.stadium}</td>
                        <td>{matchView && matchView?.dateOfMatch}</td>
                        <td>
                            {/* <div style={{ width: '100%', justifyContent: 'center', background: '#3b5998', display: 'flex' }}> */}
                                <FacebookShareButton style={{borderRadius: '50%'}}
                                    quote={`${matchView?.ename} vs ${matchView?.name}`}
                                    hashtag="#EOPFT"
                                    url={`https://www.eop.digital/detail/${getId.id}`}
                                >
                                    <FacebookIcon size={40} />
                                </FacebookShareButton>
                                {/* </div> */}
                        </td>
                    </tr>

                </tbody>
            </Table>
            {/* <div style={{ width: '100%', justifyContent: 'center', background: '#3b5998', display: 'flex' }}>
                <FacebookShareButton
                    // quote={`${playerData?.name}`}
                    hashtag="#EOPFT"
                    url={`https://www.eop.digital/detail/${getId.id}`}
                // imageURL={playerData?.images}
                //    style={{width:'100px '}}
                >
                    <FacebookIcon size={40} />
                </FacebookShareButton></div> */}
        </Container>
    )
}

export default Detail