import React, { useState } from "react";
import { Container, Image } from "react-bootstrap";

const Photos = [
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14125483_1269259363119155_7029255114593477527_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14305393_1269258986452526_785314217701318032_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/13243705_1269259193119172_5758534902090228226_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14207692_1269159853129106_1399323394202510066_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14211963_1267186376659787_6871793263774811231_n.png",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14241529_1269259416452483_7058624979677932011_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14310305_1269258989785859_2970362244057693104_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14355101_1270267166351708_2202588593319674891_n.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14310507_1269259599785798_2732476378245137404_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14355101_1270267166351708_2202588593319674891_n.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14322338_1271044036274021_7366070865664077932_n.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/13243705_1269259193119172_5758534902090228226_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14207692_1269159853129106_1399323394202510066_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14211963_1267186376659787_6871793263774811231_n.png",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14241529_1269259416452483_7058624979677932011_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14310305_1269258989785859_2970362244057693104_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14355101_1270267166351708_2202588593319674891_n.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14310507_1269259599785798_2732476378245137404_o.jpg",
  "http://www.boeungketfc.com/wp-content/uploads/photo-gallery/thumb/14355101_1270267166351708_2202588593319674891_n.jpg",
];

const Media = () => {
  console.log("Photos", Photos);
  return (
    <Container>
      <h3
        style={{
          height: "60px",
          border: "1px solid gray",
          marginTop: "100px",
          textAlign: "center",
          lineHeight: "2",
        }}
      >
        All Photos
      </h3>
      <div style={{marginBottom:'60px'}} />
      <div className="wrap-all-photos">
        {Photos?.map((a) => {
          return (
            <div
              style={{ width: "200px", height: "200px", margin:'5px 5px' }}
            >
              <Image
                className="photo"
                alt="Player Image"
                src={a}
              />
            </div>
          );
        })}
      </div>
      <div style={{marginBottom:'80px'}} />
    </Container>
  );
};
export default Media;
