import React from 'react';
// import sections
import Hero from '../components/sections/Hero';
import FeaturesTiles from '../components/sections/FeaturesTiles';
import FeaturesSplit from '../components/sections/FeaturesSplit';
import Testimonial from '../components/sections/Testimonial';
import Cta from '../components/sections/Cta';
import SlideShow from '../components/sections/SlideShow';
import Sponsor from './Sponsor';

const Home = () => {

  return (
    <>
      {/* <Hero className="illustration-section-01" /> */}
      <div style={{marginTop:'100px'}} />
      <SlideShow />
      {/* <FeaturesTiles /> */}
      <FeaturesSplit invertMobile topDivider imageFill className="illustration-section-02" />
      {/* <Testimonial topDivider /> */}
      {/* <Cta split /> */}
      <Sponsor/>
    </>
  );
}

export default Home;