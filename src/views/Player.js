import React, { useEffect, useState } from "react";
import { Container, Col, Row } from "react-bootstrap";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
import { useHistory } from "react-router-dom";

import { get_all_player } from "../services/player.services";

function CardPlayer({ item }) {
  const history = useHistory();

  return (
    <>
      <Col
        xl={3}
        md={4}
        sm={12}
        xs={12}
        style={{ textAlign: "center", alignItems: "center" }}
      >
        <div
          onClick={() => history.push("/player/" + item?.id)}
          class="parent-card"
          style={{
            backgroundImage: `url(${item?.images})`,
            backgroundSize: "cover",
            backgroundPosition: "center top",
          }}
        >
          <div
            className="wrap-text-player"
            style={{
              position: "absolute",
              color: "white",
              textAlign: "center",
            }}
          >
            <div
              style={{
                textAlign: "center",
                color: "#DAA520",
                whiteSpace: "nowrap",
                margin: "0",
                justifyContent:'space-around',
                fontWeight:'600',
                fontSize:'30px'
              }}
            >
              {item?.number}
              {/* <div>{item?.name}</div> */}
            </div>
            <p
              style={{
                fontSize: "14px",
                fontWeight: "600",
                marginBottom: "5px",
                marginTop:'-5px'
              }}
            >
              {item?.name}
              {/* {item?.position} */}
            </p>
            <div className="text-hidden">
              <Row>
                <Col
                  style={{
                    fontWeight: "600",
                    marginBottom: "3px",
                  }}
                >
                  Height
                </Col>
                <Col
                  style={{
                    fontWeight: "600",
                    marginBottom: "3px",
                  }}
                >
                  Weight
                </Col>
              </Row>
              <Row>
                <Col
                  style={{
                    fontSize: "11px",
                  }}
                >
                  {item?.height}
                </Col>
                <Col
                  style={{
                    fontSize: "11px",
                  }}
                >
                  {item?.weight}
                </Col>
              </Row>
            </div>
          </div>
          <div class="child-card-mask"></div>
        </div>
      </Col>
    </>
  );
}

const Player = () => {
  const [allPlayer, setAllPlayer] = useState();
  // const [isLoading, setIsLoading] = useState(true);

  const onGetPlayer = async () => {
    const player = await get_all_player();
    setAllPlayer(player);
  };

  // console.log("=================> player :", allPlayer);

  useEffect(() => {
    onGetPlayer();
  }, []);

  // setTimeout(() => {
  //   setIsLoading(false);
  // }, 3000);

  return (
    <Container style={{ marginTop: "100px" }}>
      <Tabs
        defaultActiveKey="home"
        transition={false}
        id="noanim-tab-example"
        className="mb-3"
      >
        {/* ----- team A ---------- */}
        <Tab eventKey="home" title="Players">
          <h4 className="main-title-header" style={{ marginTop: "10px" }}>
          GOALKEEPERS
          </h4>
          <Row className="container justify-content-center p-0 d-flex">
            {allPlayer?.map((item) => {
              if (item?.position === "Goalkeeper" && item?.team === "A") {
                return <CardPlayer item={item} />;
              }
            })}
          </Row>

          <h4 className="main-title-header" style={{ marginTop: "10px"  }}>DEFENDERS</h4>
          <Row className="container justify-content-center p-0 d-flex">
            {allPlayer?.map((item) => {
              if (item?.position === "Defender" && item?.team === "A") {
                return <CardPlayer item={item} />;
              }
            })}
          </Row>

          <h4 className="main-title-header" style={{ marginTop: "10px"  }}>MIDFIELDERS</h4>
          <Row className="container justify-content-center p-0 d-flex">
            {allPlayer?.map((item) => {
              if (item?.position === "Midfielder" && item?.team === "A") {
                return <CardPlayer item={item} />;
              }
            })}
          </Row>

          <h4 className="main-title-header" style={{ marginTop: "10px"  }}>FORWARDS</h4>
          <Row className="container justify-content-center p-0 d-flex">
            {allPlayer?.map((item) => {
              if (item?.position === "Forward" && item?.team === "A") {
                return <CardPlayer item={item} />;
              }
            })}
          </Row>
        </Tab>

        {/* ----- team B ---------- */}
        <Tab eventKey="profile" title="Academy">
          <h4 className="main-title-header" style={{ marginTop: "10px",marginTop: "10px"  }}>
            Goalkeeper
          </h4>
          <Row className="container justify-content-center p-0 d-flex">
            {allPlayer?.map((item) => {
              if (item?.position === "Goalkeeper" && item?.team === "B") {
                return <CardPlayer item={item} />;
              }
            })}
          </Row>

          <h4 className="main-title-header" style={{ marginTop: "10px"  }}>Defender</h4>
          <Row className="container justify-content-center p-0 d-flex">
            {allPlayer?.map((item) => {
              if (item?.position === "Defender" && item?.team === "B") {
                return <CardPlayer item={item} />;
              }
            })}
          </Row>

          <h4 className="main-title-header" style={{ marginTop: "10px"  }}>Midfielder</h4>
          <Row className="container justify-content-center p-0 d-flex">
            {allPlayer?.map((item) => {
              if (item?.position === "Midfielder" && item?.team === "B") {
                return <CardPlayer item={item} />;
              }
            })}
          </Row>

          <h4 className="main-title-header" style={{ marginTop: "10px"  }}>Forward</h4>
          <Row className="container justify-content-center p-0 d-flex">
            {allPlayer?.map((item) => {
              if (item?.position === "Forward" && item?.team === "B") {
                return <CardPlayer item={item} />;
              }
            })}
          </Row>
        </Tab>
      </Tabs>
      <div className="before_footer" />
    </Container>
  );
};
export default Player;
