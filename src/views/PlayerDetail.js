import React, { useState, useEffect } from "react";
import { Row, Col, Image, Container } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { FacebookIcon, FacebookShareButton } from "react-share";

import { get_player_by_id } from "../services/player.services";

const PlayerDetail = () => {
  const [playerData, setPlayerData] = useState();

  const param = useParams();
  let id = param?.id;

  const name = playerData?.name.toUpperCase();

  const onGetPlayerById = async () => {
    const player = await get_player_by_id(id);
    setPlayerData(player);
  };

  useEffect(() => {
    onGetPlayerById();
  }, [id]);

  // console.log("==============>", playerData);

  return (
    <Container style={{ marginTop: "100px", marginBottom: "40px" }}>
      <h4
        className="main-title-header"
        style={{ marginTop: "10px",marginBottom: "30px" }}
      >
        {playerData?.position.toUpperCase()}
      </h4>
      <Row>
        <Col lg="6" md="12" sm="12" xs="12">
          <Image
            className="player-profile"
            alt="Player Image"
            src={`${playerData?.images}`}
          />
        </Col>
        <Col lg="6" md="12" sm="12" xs="12">
          <Row>
            <Col>
              <h4>{name}</h4>
            </Col>
            <Col>
              <h4>{playerData?.number}</h4>
            </Col>
          </Row>
          <Row className="player-data-list">
            <Col>Position</Col>
            <Col className="player-des"> &nbsp; {playerData?.position}</Col>
          </Row>
          <Row className="player-data-list">
            <Col>Place of birth</Col>
            <Col className="player-des"> &nbsp; {playerData?.placeOfBirth}</Col>
          </Row>
          <Row className="player-data-list">
            <Col>Height</Col>
            <Col className="player-des"> &nbsp; {playerData?.height}</Col>
          </Row>
          <Row className="player-data-list">
            <Col>Weight</Col>
            <Col className="player-des"> &nbsp; {playerData?.weight}</Col>
          </Row>
          <Row className="player-data-list">
            <Col>Join EOP</Col>
            <Col className="player-des"> &nbsp; {playerData?.jointed}</Col>
          </Row>
          <Row className="player-data-list">
            <Col>Best foot</Col>
            <Col className="player-des"> &nbsp; {playerData?.bestFoot}</Col>
          </Row>
          <h6 style={{ margin: "10px 0" }}>Share to</h6>
          <FacebookShareButton
            quote={`${playerData?.name} ${playerData?.number}`}
            hashtag="#EOPFT"
            url={`https://www.eop.digital/player/${id}`}
          >
            <FacebookIcon size={40} />
          </FacebookShareButton>
        </Col>
      </Row>
    </Container>
  );
};

export default PlayerDetail;
