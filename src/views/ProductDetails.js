import React, { useState, useEffect } from "react";
import { Row, Col, Image, Container } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { FacebookIcon, FacebookShareButton } from "react-share";

import { get_product_by_id } from "../services/product.services";

const ProductDetails = () => {
  const [productData, setProductData] = useState();

  const param = useParams();
  let id = param?.id;

  const name = productData?.name.toUpperCase();

  const onGetProductById = async () => {
    const product = await get_product_by_id(id);
    setProductData(product);
  };

  useEffect(() => {
    onGetProductById();
  }, [id]);

  // console.log("==============>", productData);

  return (
    <Container style={{ marginTop: "100px", marginBottom: "40px" }}>
      <h4 className="main-title-header" style={{ marginTop: "10px",marginBottom:'20px' }}>
        EOP Merchandise
      </h4>
      <Row>
        <Col lg="6" md="12" sm="12" xs="12">
          <Image
            className="player-profile"
            alt="Player Image"
            src={`${productData?.images}`}
          />
        </Col>

        <Col lg="6" md="12" sm="12" xs="12">
          <h6 style={{ margin: "0", marginTop: "10px", marginBottom: "5px" }}>
          {name}
          </h6>
          <p style={{ fontSize: "15px" }}>Price : {productData?.price}</p>
          <h6 style={{ margin: "10px 0" }}>Buy Now</h6>
          <p style={{ fontSize: "15px" }}>{productData?.description}</p>
          <h6 style={{ margin: "10px 0" }}>Share to</h6>
          <FacebookShareButton
            quote={`${productData?.name}`}
            hashtag="#EOPFT"
            url={`https://www.eop.digital/store/${id}`}
            imageURL={productData?.images}
          >
            <FacebookIcon size={40} />
          </FacebookShareButton>
        </Col>
      </Row>
    </Container>
  );
};

export default ProductDetails;
