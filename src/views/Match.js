import React, { useEffect, useState } from "react";
import { Image, Row, Col, Table, Tabs, Tab } from "react-bootstrap";
import { useHistory } from 'react-router-dom'
import { get_all_match } from "../services/match.services";

const Match = () => {

	let history = useHistory();

	const [allMatch, setAllMatch] = useState();
	const [lose, setLose] = useState([]);
	const [equal, setEqual] = useState([]);
	const [win, setWin] = useState([]);

	const onGetMatch = async () => {
		const matches = await get_all_match();
		setAllMatch(matches);
		setLose(matches.filter(matches => matches.lresult === 'lose'))
		setEqual(matches.filter(matches => matches.lresult === 'equal'))
		setWin(matches.filter(matches => matches.lresult === 'win'))
	};

	useEffect(() => {
		onGetMatch();
	}, []);
	// console.log("=================> match :", allMatch);

	const NumberOfMatchs = () =>{
		return(
			<>
				<div className="item-result">
					<div className="item-result-box" style={{ borderRight: 'none', backgroundColor: '#0047AB', color: '#fff' , borderBottom: 'none'}}>Matches</div>
					<div className="item-result-box" style={{ borderLeft: 'none', color: '#DAA520', borderBottom: 'none' }}>{allMatch?.length}</div>
				</div>
				<div className="item-result">
					<div className="item-result-box" style={{ borderRight: 'none', backgroundColor: '#0047AB', color: '#fff' , borderBottom: 'none'}}>Win</div>
					<div className="item-result-box" style={{ borderLeft: 'none', color: '#DAA520', borderBottom: 'none' }}>{win?.length}</div>
				</div>
				<div className="item-result">
					<div className="item-result-box" style={{ borderRight: 'none', backgroundColor: '#0047AB', color: '#fff' , borderBottom: 'none'}}>Lose</div>
					<div className="item-result-box" style={{ borderLeft: 'none', color: 'red', borderBottom: 'none' }}>{lose?.length}</div>
				</div>
				<div className="item-result">
					<div className="item-result-box" style={{ borderRight: 'none', backgroundColor: '#0047AB', color: '#fff' , borderBottom: 'none'}}>Equal</div>
					<div className="item-result-box" style={{ borderLeft: 'none', color: '#DAA520' }}>{equal?.length}</div>
				</div>
			</>
		);
	}

	const TableData = ({ item }) => {
		return (
			<tr style={{ height: '40px',cursor:'pointer', borderBottomWidth: '0px !important' }} onClick={() => history.push("/detail/" + item?.id)}>
				<td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
					<div className="match">{item.ename}</div>
				</td>
				<td style={{ width: '60px' }}>
					<div style={{ width: '60px', height: '40px' }}>
						<Image src={item.elogo}
							style={{ width: "auto", height: "40px", display: 'block', marginLeft: "auto", marginRight: 'auto' }}
						/>
					</div>
				</td>
				<td style={{ textAlign: 'center', verticalAlign: 'middle' }} width='70px'>{item.result}</td>
				<td style={{ width: '60px' }}>
					<div style={{ width: '60px', height: '40px' }}>
						<Image src={item.logo}
							style={{ width: "auto", height: "40px", display: 'block', marginLeft: "auto", marginRight: 'auto' }}
						/>
					</div>
				</td>
				<td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
					<div className="match">{item.name}</div>
				</td>
				<td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
					<div className="match">{item.dateOfMatch}</div>
				</td>
			</tr>
		)
	}

	return (
		<div class="container wrap-table" >
			<Tabs defaultActiveKey="all" id="uncontrolled-tab-example" style={{ marginTop: "100px" }}>

				{/* ================== All match ================= */}
				<Tab eventKey="all" title="All">
					<Row className="mb-3 mt-3">
						<Col lg={3} md={3} sm={12} xs={12}
							className="match"
							style={{ padding: '0px 15px 25px 15px' }}
						>
							<NumberOfMatchs />
						</Col>
						<Col lg={9} md={9} sm={12} xs={12}>
							<div className="wrap-table">
								<Table striped hover responsive size="md" borderless={true}>
									<tbody >
										{allMatch?.map((item, i) => {
											
												return (
													<TableData item={item} key={i} />
												)
										})}
									</tbody>
								</Table>
							</div>
						</Col>
					</Row>
				</Tab>

				{/* ================== Friendly match ================= */}
				<Tab eventKey="friendly" title="Friendly">
					<Row className="mb-3 mt-3">
						<Col lg={3} md={3} sm={12} xs={12}
							className="match"
							style={{ padding: '0px 15px 25px 15px' }}
						>
							<NumberOfMatchs />
						</Col>
						<Col lg={9} md={9} sm={12} xs={12}>
							<div className="wrap-table">
								<Table striped hover responsive>
									<tbody >
										{allMatch?.map((item, i) => {
											if (item.type == "Friendly") {
												return (
													<TableData item={item} key={i} />
												)
											}
										})}
									</tbody>
								</Table>
							</div>
						</Col>
					</Row>
				</Tab>

				{/* ================== CFutsal match ================= */}
				<Tab eventKey="CFutsal" title="CFutsal">
					<Row className="mb-3 mt-3">
						<Col lg={3} md={3} sm={12} xs={12}
							className="match"
							style={{ padding: '0px 15px 25px 15px' }}
						>
							<NumberOfMatchs />
						</Col>
						<Col lg={9} md={9} sm={12} xs={12}>
							<div className="wrap-table">
								<Table striped hover responsive>
									<tbody >
										{allMatch?.map((item, i) => {
											if (item.type == "CFutsal") {
												return (
													<TableData item={item} key={i} />
												)
											}
										})}
									</tbody>
								</Table>
							</div>
						</Col>
					</Row>
				</Tab>

				{/* ================== MakorU23 match ================= */}
				<Tab eventKey="MakorU23" title="MakorU23">
					<Row className="mb-3 mt-3">
						<Col lg={3} md={3} sm={12} xs={12}
							className="match"
							style={{ padding: '0px 15px 25px 15px' }}
						>
							<NumberOfMatchs />
						</Col>
						<Col lg={9} md={9} sm={12} xs={12}>
							<div className="wrap-table">
								<Table striped hover responsive>
									<tbody >
										{allMatch?.map((item, i) => {
											if (item.type == "MakorU23") {
												return (
													<TableData item={item} key={i} />
												)
											}
										})}
									</tbody>
								</Table>
							</div>
						</Col>
					</Row>
				</Tab>
			</Tabs>
		</div>
	);
};
export default Match;
