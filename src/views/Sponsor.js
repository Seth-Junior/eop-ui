import React from 'react';
import { Row, Col } from 'react-bootstrap';


export default function () {

    const sponsorData = [
        'https://assets.manutd.com/AssetPicker/images/0/0/11/53/734544/Adidas_Comp_LightBG_180xAuto1547460175451.png',
        'https://assets.manutd.com/AssetPicker/images/0/0/12/52/799813/Konami_PartnerFooter1562073557221.png',
        'https://assets.manutd.com/AssetPicker/images/0/0/11/53/734554/Chevrolet_Comp_LightBG_180xAuto1547460485843.png',
        'https://www.cgcc.com.kh/wp-content/uploads/2021/06/PFIs-logo-03.png',
    ]


    return (
        <div class="container">
            <div className="sponsor">
                {sponsorData.map((item)=>
                    <img src={item} className="sponser-img"/>
                )}
            </div>
        </div>
    )
}
