import { API } from "../utils/api"

export const get_all_player = async () => {
    try {
        const getAllPlayer = await API.get('/player')
        // console.log('All player :',getAllPlayer.data.data);
        return getAllPlayer.data.data
    } catch (error) {
        console.log('Error');
    }
}

export const get_player_by_id = async (id) => {
    try {
        const getPlayerById = await API.get(`/player/${id}/view`)
        // console.log('get_player_by_id :',getPlayerById.data.data);
        return getPlayerById.data.data
    } catch (error) {
        console.log('Error');
    }
}