import { API } from "../utils/api"

export const get_all_product = async () => {
    try {
        const getAllProduct = await API.get('/store')
        // console.log('getAllProduct :',getAllProduct.data.data);
        return getAllProduct.data.data
    } catch (error) {
        console.log('Error');
    }
}

export const get_product_by_id = async (id) => {
    try {
        const getProductById = await API.get(`/store/${id}/view`)
        console.log('getProductById :',getProductById.data.data);
        return getProductById.data.data
    } catch (error) {
        console.log('Error');
    }
}