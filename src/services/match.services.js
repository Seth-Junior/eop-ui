import { API } from "../utils/api"

export const get_all_match = async () => {
    try {
        const getAllMatch = await API.get('/match')
        // console.log('All matches :', getAllMatch.data.data);
        return getAllMatch.data.data
    } catch (error) {
        console.log('Error', error);
    }
}

export const get_View_Match_ById = async (id) => {
    try {
        const getViewMatchById = await API.get(`/match/${id}/view`);
        // console.log('View match by ID : ', getViewMatchById.data.data);
        return getViewMatchById.data.data
    } catch (error) {
        console.log('Error', error);
    }
}