import React from "react";
import classNames from "classnames";
import { FaFacebookSquare, FaInstagram  } from "react-icons/fa";
import { SiTwitter } from "react-icons/si";

const FooterDes = ({ className, ...props }) => {
  const classes = classNames("footer-social", className);

  return (
    <div {...props} className={classes}>
      <h6 style={{color:'white'}}>We are EOP FT</h6>
      <div style={{ display: "flex" , justifyContent:'center' }}>
        <div className="footer-socail-card team-motivational-txt">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
        </div>
      </div>
    </div>
  );
};

export default FooterDes;
