import React from "react";
import classNames from "classnames";
import { FaFacebookSquare, FaInstagram  } from "react-icons/fa";
import { SiTwitter } from "react-icons/si";

const FooterSocial = ({ className, ...props }) => {
  const classes = classNames("footer-social", className);

  return (
    <div {...props} className={classes}>
      <h6 style={{color:'white'}}>Follow EOP FT on socail media</h6>
      <div style={{ display: "flex" , justifyContent:'center' }}>
        <div className="footer-socail-card">
          <a
            style={{ display: "block" }}
            target="_blank"
            href="https://www.facebook.com/EOP.FT"
          >
            <FaFacebookSquare style={{ fontSize: "20px", color: "white" }} />
            <p style={{ color: "white", fontSize: "11px", marginTop: "3px" }}>
              @EOP.FT
            </p>
          </a>
        </div>
        <div className="footer-socail-card instagram">
          <a
            style={{ display: "block" }}
            target="_blank"
            href="https://www.facebook.com/EOP.FT"
          >
            <FaInstagram style={{ fontSize: "20px", color: "white" }} />
            <p style={{ color: "white", fontSize: "11px", marginTop: "3px" }}>
              @EOP.FT
            </p>
          </a>
        </div>
        <div className="footer-socail-card twitter">
          <a
            style={{ display: "block" }}
            target="_blank"
            href="https://www.facebook.com/EOP.FT"
          >
            <SiTwitter style={{ fontSize: "20px", color: "white" }} />
            <p style={{ color: "white", fontSize: "11px", marginTop: "3px" }}>
              @EOP.FT
            </p>
          </a>
        </div>
      </div>
    </div>
  );
};

export default FooterSocial;
