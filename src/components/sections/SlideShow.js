import React, { useState } from "react";
import { Carousel, Container } from "react-bootstrap";

const SlideData = [
  'https://scontent-mrs2-1.xx.fbcdn.net/v/t1.6435-9/152310616_3925829690826675_5509780089143956275_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=0debeb&_nc_eui2=AeFXtNczF-328SOk8prRBwrWUwPKNtgdy1JTA8o22B3LUiA91EB-ugCphQKoOdG1KZQ6gcDs0ROMQ3BAKFaF7BA5&_nc_ohc=2dkUaeaGIKEAX-jwFXT&_nc_ht=scontent-mrs2-1.xx&oh=00_AT_betKUMVsn2FL7Zxt5USbijhRUBF4bw8-D8v53tHQ74g&oe=626CA45A',
  'https://scontent-mrs2-2.xx.fbcdn.net/v/t1.6435-9/120806335_3498050803604568_9093377223092233559_n.jpg?_nc_cat=101&ccb=1-5&_nc_sid=e3f864&_nc_eui2=AeGoixnYujiXbc7CzSnFdy8cnuILtQX4kjie4gu1BfiSOPhJ6JqxkkeqCqn4kyJXbw03i00lm7u-zlWIsOmvcdyT&_nc_ohc=hNOryvmmgbAAX97zxc8&tn=voO6fEIbx9LS7DuB&_nc_ht=scontent-mrs2-2.xx&oh=00_AT_JL8VQTAom8DpPX_Mp6lqZc9HG3MgrWdVHKBptO9IkTA&oe=626BE0FE',
  'https://scontent-mrs2-2.xx.fbcdn.net/v/t39.30808-6/275643884_5122024014540564_34645549016693408_n.jpg?_nc_cat=102&ccb=1-5&_nc_sid=8bfeb9&_nc_eui2=AeE9QagmRmK1JksSSr0KblqJacCANWfpjw1pwIA1Z-mPDdNoswF-wLtaQFqOHPqXH9MkQ7ge6-QcoXlG1aW5jYYo&_nc_ohc=ftAHr-YoaUMAX-z52Bt&_nc_ht=scontent-mrs2-2.xx&oh=00_AT_z3Wd0NlOmhL5eOVCLDXEVzxCjPQZ0KKIvepkHoMpqUg&oe=624A7282',
  'https://scontent-mrs2-2.xx.fbcdn.net/v/t39.30808-6/273839297_5037646179645015_7684788877537173255_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=8bfeb9&_nc_eui2=AeGIUvb1Ua9zaz9FAnL6qBm_mcVOESGv-16ZxU4RIa_7XuO_lsiLgJ2w_xSb2gJEfe6V1U1b9SsCokltOqVkbPTx&_nc_ohc=j3ajw5GnILAAX9byDuG&_nc_ht=scontent-mrs2-2.xx&oh=00_AT9RL9D6mPp8aHXlTS6bNaOjvQy7HwQ5_jtM4VetgVK-8w&oe=624A0384',
  'https://scontent-mrs2-1.xx.fbcdn.net/v/t39.30808-6/272124983_4952076984868602_8939123285621655119_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=8bfeb9&_nc_eui2=AeFNu8uMHK5gnHOL1Dg_5k977JNQU1wKWA_sk1BTXApYD9EuLvNyl2dPLm0Ba9TOLpYZGp4DWIBwAYmC4pzquG8Y&_nc_ohc=M4ftbjPqM_8AX902mG8&tn=voO6fEIbx9LS7DuB&_nc_ht=scontent-mrs2-1.xx&oh=00_AT_12U-Gt3ZpInwkTXKSe4L5ErOXcbHMyovqg-ZTHy-W_g&oe=6249AC22',
  'https://scontent-mrs2-2.xx.fbcdn.net/v/t39.30808-6/271584379_4924242127652088_4796868389341120247_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=8bfeb9&_nc_eui2=AeFL2BEcHvsl_Usxn2pCL97UWFhIvwhOwBJYWEi_CE7AEo4yXji5AZaOV0kvdEIY5GXtsmkpOb9eLsmcCeZBka8J&_nc_ohc=O8sApZYwfWoAX9nOhO8&tn=voO6fEIbx9LS7DuB&_nc_ht=scontent-mrs2-2.xx&oh=00_AT_w-YvT7eAYh7fcjxJvW7ieeFgVTm78iEy6wQhTE-9V3Q&oe=624A7273',
]

const SlideShow = () => {

  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };


  return (
    <div style={{ marginTop: "30px" }}>
      <Carousel activeIndex={index} onSelect={handleSelect} >
        {
          SlideData?.map((val) => {
            return (
              <Carousel.Item>
                <div className="wrap-slide">
                  <img
                    className="d-block w-100 image-slide"
                    src={val}
                    alt="First slide"
                  />
                </div>
                {/* <Carousel.Caption>
                  <h3>First slide label</h3>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption> */}
              </Carousel.Item>
            )
          })
        }
      </Carousel>
      {/* <Carousel.Item interval={2000}>
          <img
            className="d-block w-100 image-slide"
            src="https://wallpaperaccess.com/full/2163567.jpg"
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100​ image-slide"
            src="https://wallpaperaccess.com/full/2163567.jpg"
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100 image-slide"
            src="https://wallpaperaccess.com/full/2163567.jpg"
            alt="Third slide"
          />
        </Carousel.Item> */}

    </div>
  );
};
export default SlideShow;
